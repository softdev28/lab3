import 'dart:io';

import 'package:lab3/lab3.dart' as lab3;

void main(List<String> arguments) {
  String? words = stdin.readLineSync()!;
  words.toLowerCase();
  List<String> wordLowerCaseList = words.split(' ');

  var map = {};

  for (var word in wordLowerCaseList) {
    if (!map.containsKey(word)) {
      map[word] = 1;
    } else {
      map[word] += 1;
    }
  }

  print(map);
}
